# TP5 - Tests unitaires

## Sujet

### Projet de base
  - faites un fork du projet
  - clonez le sur votre poste

### Intégration continue
  - créez un fichier .gitlab-ci.yml à la racine de votre projet
  - ajoutez un script pour executer les tests en respectant le format suivant :

```
image: maven:latest
run_test:
    script:
        - sh script/test.sh
```
  - pushez la modification
  - dans l'interface gitlab, menu "build", un job a du être exécuté

### Mains libres:
  - vous avez entre les mains une version "horror" et buggée du jeu des allumettes. Améliorez la lisibilité, redigez tous les tests pertinents puis ajoutez le code nécessaire pour les valider. 
  - votre projet doit être versionné avec git
  - objectif : une couverture de test parfaite et une justification pour les portions de codes non testées

## Le jeu des allumettes

Alignez autant d'allumettes que vous le souhaitez sur une table.
Chacun son tour, un joueur retire 1, 2 ou 3 allumettes.
Le joueur qui retire la dernière allumette a perdu.




